VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ファイル名接頭連番リネーム"
   ClientHeight    =   5385
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12165
   LinkTopic       =   "MainForm"
   MaxButton       =   0   'False
   ScaleHeight     =   359
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   811
   StartUpPosition =   3  'Windows の既定値
   Begin VB.CheckBox chkDoubleChange 
      Caption         =   "二重変換"
      Height          =   375
      Left            =   8520
      TabIndex        =   16
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "除去"
      Height          =   375
      Left            =   7320
      TabIndex        =   15
      Top             =   4920
      Width           =   975
   End
   Begin VB.CommandButton cmdDown 
      Caption         =   "下へ"
      Height          =   375
      Left            =   6240
      TabIndex        =   14
      Top             =   4920
      Width           =   975
   End
   Begin VB.CommandButton cmdUp 
      Caption         =   "上へ"
      Height          =   375
      Left            =   5160
      TabIndex        =   13
      Top             =   4920
      Width           =   975
   End
   Begin VB.CommandButton cmdRestore 
      Caption         =   "戻す"
      Enabled         =   0   'False
      Height          =   375
      Left            =   9720
      TabIndex        =   11
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton cmdExcute 
      Caption         =   "実行"
      Height          =   375
      Left            =   10920
      TabIndex        =   10
      Top             =   600
      Width           =   1095
   End
   Begin VB.CommandButton cmdTest 
      Caption         =   "確認"
      Height          =   375
      Left            =   9720
      TabIndex        =   9
      Top             =   600
      Width           =   1095
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "クリア"
      Height          =   375
      Left            =   8520
      TabIndex        =   8
      Top             =   4920
      Width           =   1095
   End
   Begin VB.TextBox txtNumberLength 
      Height          =   375
      Left            =   7320
      MaxLength       =   2
      TabIndex        =   7
      Text            =   "3"
      Top             =   600
      Width           =   855
   End
   Begin VB.TextBox txtStartNumber 
      Height          =   375
      IMEMode         =   3  'ｵﾌ固定
      Left            =   7320
      MaxLength       =   6
      TabIndex        =   5
      Text            =   "1"
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox txtBeforeNumberLength 
      Height          =   375
      IMEMode         =   3  'ｵﾌ固定
      Left            =   3000
      MaxLength       =   2
      TabIndex        =   3
      Text            =   "3"
      Top             =   600
      Width           =   855
   End
   Begin VB.CheckBox chkEnableNumber 
      Caption         =   "Beforeに接頭番号あり:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Value           =   1  'ﾁｪｯｸ
      Width           =   2175
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      OLEDropMode     =   1
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      OLEDropMode     =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Key             =   "BEFORE"
         Text            =   "Before"
         Object.Width           =   6068
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Key             =   "AFTER"
         Text            =   "After"
         Object.Width           =   6068
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Key             =   "STATUS"
         Text            =   "Status"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "※エクスプローラー等からファイルをドラッグ＆ドロップ"
      Height          =   180
      Left            =   120
      TabIndex        =   12
      Top             =   4920
      Width           =   4080
   End
   Begin VB.Label lblNumberLength 
      AutoSize        =   -1  'True
      Caption         =   "数字の桁数（スペース含まない）:"
      Height          =   180
      Left            =   4680
      TabIndex        =   6
      Top             =   600
      Width           =   2460
   End
   Begin VB.Label lblStartNumber 
      AutoSize        =   -1  'True
      Caption         =   "連番スタート値:"
      Height          =   180
      Left            =   5880
      TabIndex        =   4
      Top             =   120
      Width           =   1170
   End
   Begin VB.Label lblBeforeNumberLength 
      AutoSize        =   -1  'True
      Caption         =   "数字の桁数(スペース等含む):"
      Height          =   180
      Left            =   600
      TabIndex        =   2
      Top             =   600
      Width           =   2265
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim FSO As New FileSystemObject
Dim mblnExcuted As Boolean

Private Sub ChangeExcutedState()
    cmdExcute.Enabled = Not mblnExcuted
    cmdRestore.Enabled = mblnExcuted
End Sub

Private Sub FormatTextNumber(aTextBox As TextBox, aMaxNumber As Integer, aMinNumber As Integer)
    Dim s As String, d As Double
    s = txtBeforeNumberLength.Text
    If IsNumeric(s) Then
        d = Val(s)
        If d > CDbl(aMaxNumber) Then
            d = CDbl(aMaxNumber)
        ElseIf d < CDbl(aMinNumber) Then
            d = CDbl(aMinNumber)
        Else
            d = Int(d)
        End If
        txtBeforeNumberLength.Text = CStr(CInt(d))
    Else
        s = CStr(aMinNumber)
    End If
End Sub

Private Sub MyLockControls(ByVal ablnLocked As Boolean)
    Dim blnUnLock As Boolean
    
    blnUnLock = Not ablnLocked
    
    chkEnableNumber.Enabled = blnUnLock
    
    ListView1.Enabled = blnUnLock
    
    chkDoubleChange.Enabled = blnUnLock
    
    cmdClear.Enabled = blnUnLock
    cmdTest.Enabled = blnUnLock
    
    cmdUp.Enabled = blnUnLock
    cmdDown.Enabled = blnUnLock
    cmdRemove.Enabled = blnUnLock
    
    If blnUnLock Then
        Call chkEnableNumber_Click
        Call ChangeExcutedState
    Else
        lblBeforeNumberLength.Enabled = False
        txtBeforeNumberLength.Enabled = False
        cmdExcute.Enabled = False
        cmdRestore.Enabled = False
    End If
    
End Sub

Private Function ChangeName(ByVal astrName As String, ByVal aintIndex As Integer) As String
    Dim ln As Long
    Dim st As Integer
    Dim nm As Integer
    Dim rm As Integer
    
    ln = CLng(Val(txtNumberLength.Text))
    st = CInt(Val(txtStartNumber.Text))
    nm = st + aintIndex
    
    If chkEnableNumber.Value = Checked Then
        rm = CInt(Val(txtBeforeNumberLength.Text))
        If Len(astrName) > rm Then
            astrName = Mid$(astrName, rm + 1)
        Else
            astrName = ""
        End If
    End If
    
    ChangeName = Right$(String(ln, "0") & CStr(nm), ln) & " " & Trim$(astrName)
    
End Function



Private Sub ChangeFilesName(ByVal ablnTest As Boolean)
    Dim it As ListItem
    Dim i As Integer
    Dim cn As String
    Dim cp As String
    Dim ex As String
    Dim pt As String
    
    For i = 1 To ListView1.ListItems.Count
        Set it = ListView1.ListItems.Item(i)
        pt = it.Tag
        cn = ChangeName(it.Text, i - 1)
        it.ListSubItems("AFTER").Text = cn
        cp = FSO.BuildPath(FSO.GetParentFolderName(pt), cn)
        ex = FSO.GetExtensionName(pt)
        If Len(ex) > 0& Then
            cp = cp & "." & ex
        End If
        If FSO.FileExists(pt) Then
            If ablnTest Then
                it.ListSubItems("STATUS").Text = IIf(FSO.FileExists(cp), "NG", "OK")
            Else
                If FSO.FileExists(cp) Then
                    it.ListSubItems("STATUS").Text = "FAILURE EXISTED"
                Else
                    FSO.MoveFile pt, cp
                    If FSO.FileExists(cp) Then
                        it.Tag = cp
                        it.Key = cp
                        it.ListSubItems("STATUS").Text = "SUCCESS"
                    Else
                        it.ListSubItems("STATUS").Text = "FAILURE"
                    End If
                End If
            End If
        Else
            it.ListSubItems("STATUS").Text = "NOT FOUND"
        End If
    Next i
    
    mblnExcuted = Not ablnTest
    
End Sub

Private Sub DoubleChangeFilesName(ByVal ablnTest As Boolean)
    Dim it As ListItem, it2 As ListItem
    Dim i As Integer
    Dim cn As String
    Dim cp As String
    Dim ex As String
    Dim pt As String
    Dim tmp As String
    Dim rd As New Dictionary
    
    If Not ablnTest Then
        For i = 1 To ListView1.ListItems.Count
            Set it = ListView1.ListItems.Item(i)
            Do
                tmp = FSO.BuildPath(FSO.GetParentFolderName(it.Tag), FSO.GetBaseName(FSO.GetTempName()))
                ex = FSO.GetExtensionName(it.Tag)
                If Len(ex) > 0& Then
                    tmp = tmp & "." & ex
                End If
            Loop While FSO.FileExists(tmp)
            If FSO.FileExists(it.Tag) Then
                FSO.MoveFile it.Tag, tmp
                rd.Add Key:=tmp, Item:=it.Tag
                it.Tag = tmp
                it.Key = tmp
            End If
        Next i
    End If
    
    For i = 1 To ListView1.ListItems.Count
        Set it = ListView1.ListItems.Item(i)
        pt = it.Tag
        cn = ChangeName(it.Text, i - 1)
        it.ListSubItems("AFTER").Text = cn
        cp = FSO.BuildPath(FSO.GetParentFolderName(pt), cn)
        ex = FSO.GetExtensionName(pt)
        If Len(ex) > 0& Then
            cp = cp & "." & ex
        End If
        If FSO.FileExists(pt) Then
            If ablnTest Then
                If FSO.FileExists(cp) Then
                    On Error Resume Next
                    Err.Clear
                    Set it2 = ListView1.ListItems(cp)
                    If Err.Number <> 0 Then
                        If Err.Number = ccElemNotFound Then
                            it.ListSubItems("STATUS").Text = "NG"
                            Err.Clear
                        Else
                            Err.Raise Err.Number
                        End If
                    Else
                        it.ListSubItems("STATUS").Text = "OK"
                    End If
                    On Error GoTo 0
                Else
                    it.ListSubItems("STATUS").Text = "OK"
                End If
            Else
                If FSO.FileExists(cp) Then
                    it.ListSubItems("STATUS").Text = "FAILURE EXISTED"
                    it.Key = rd.Item(it.Tag) ' 失敗する恐れあり（変更成功の名前との競合の恐れあり）
                    FSO.MoveFile pt, it.Key
                    it.Tag = it.Key
                Else
                    FSO.MoveFile pt, cp
                    If FSO.FileExists(cp) Then
                        it.Tag = cp
                        it.Key = cp
                        it.ListSubItems("STATUS").Text = "SUCCESS"
                    Else
                        it.ListSubItems("STATUS").Text = "FAILURE"
                        it.Key = rd.Item(it.Tag) ' 失敗する恐れあり（変更成功の名前との競合の恐れあり）
                        FSO.MoveFile pt, it.Key
                        it.Tag = it.Key
                    End If
                End If
            End If
        Else
            it.ListSubItems("STATUS").Text = "NOT FOUND"
        End If
    Next i
    
    mblnExcuted = Not ablnTest
    
End Sub

Private Sub RestoreChangeFilesName()
    Dim it As ListItem
    Dim pt As String
    Dim ex As String
    Dim sc As Integer
    Dim db As Boolean
    Dim tmp As String
    Dim rd As New Dictionary
    
    db = (chkDoubleChange.Value = vbChecked)
    
    If db Then
        For Each it In ListView1.ListItems
            
            If it.ListSubItems("STATUS").Text = "SUCCESS" Or InStr(1, it.ListSubItems("STATUS").Text, "RESTORE ") > 0 Then
                Do
                    tmp = FSO.BuildPath(FSO.GetParentFolderName(it.Tag), FSO.GetBaseName(FSO.GetTempName()))
                    ex = FSO.GetExtensionName(it.Tag)
                    If Len(ex) > 0& Then
                        tmp = tmp & "." & ex
                    End If
                Loop While FSO.FileExists(tmp)
                If FSO.FileExists(it.Tag) Then
                    FSO.MoveFile it.Tag, tmp
                    rd.Add Key:=tmp, Item:=it.Tag
                    it.Tag = tmp
                    it.Key = tmp
                End If
            End If
        Next it
    End If
    
    sc = 0
    
    For Each it In ListView1.ListItems
        
        If it.ListSubItems("STATUS").Text = "SUCCESS" Or InStr(1, it.ListSubItems("STATUS").Text, "RESTORE ") > 0 Then
            sc = sc + 1
            pt = FSO.BuildPath(FSO.GetParentFolderName(it.Tag), it.Text)
            ex = FSO.GetExtensionName(it.Tag)
            If Len(ex) > 0& Then
                pt = pt & "." & ex
            End If
            If FSO.FileExists(it.Tag) Then
                If FSO.FileExists(pt) Then
                    it.ListSubItems("STATUS").Text = "EXISTED RESTORE NAME"
                    If db Then
                        it.Key = rd.Item(it.Tag)  ' 失敗する恐れあり（戻し成功の名前との競合の恐れあり）
                        FSO.MoveFile it.Tag, it.Key
                        it.Tag = it.Key
                    End If
                Else
                    FSO.MoveFile it.Tag, pt
                    If FSO.FileExists(pt) Then
                        it.Tag = pt
                        it.Key = pt
                        it.ListSubItems("STATUS").Text = "RESTORED"
                        sc = sc - 1
                    Else
                        it.ListSubItems("STATUS").Text = "FAILED RESTORE "
                        If db Then
                            it.Key = rd.Item(it.Tag)  ' 失敗する恐れあり（戻し成功の名前との競合の恐れあり）
                            FSO.MoveFile it.Tag, it.Key
                            it.Tag = it.Key
                        End If
                    End If
                End If
            Else
                it.ListSubItems("STATUS").Text = "NOT FOUND RESTORE NAME"
            End If
        End If
        
    Next it
    
    mblnExcuted = sc <> 0
    
End Sub

Private Sub chkEnableNumber_Click()
    Dim b As Boolean
    b = (chkEnableNumber.Value = vbChecked)
    lblBeforeNumberLength.Enabled = b
    txtBeforeNumberLength.Enabled = b
    
End Sub


Private Sub cmdClear_Click()
    If MsgBox("リストをクリアします", vbYesNoCancel, "確認") <> vbYes Then
        Exit Sub
    End If
    Call MyLockControls(True)
    ListView1.ListItems.Clear
    Call MyLockControls(False)
End Sub

Private Sub cmdDown_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "リストが空です！", vbInformation Or vbOKOnly, "エラー"
        Exit Sub
    End If
    Call MyLockControls(True)
    Dim it As ListItem, it2 As ListItem, sit As ListSubItem
    Dim i As Integer
    For i = ListView1.ListItems.Count To 1 Step -1
        Set it = ListView1.ListItems.Item(i)
        If it.Selected Then
            If i < ListView1.ListItems.Count Then
                ListView1.ListItems.Remove i
                Set it2 = ListView1.ListItems.Add(Index:=i + 1, Key:=it.Key, Text:=it.Text)
                Set sit = it.ListSubItems("AFTER")
                it2.ListSubItems.Add Index:=sit.Index, Key:=sit.Key, Text:=sit.Text
                Set sit = it.ListSubItems("STATUS")
                it2.ListSubItems.Add Index:=sit.Index, Key:=sit.Key, Text:=sit.Text
                it2.Tag = it.Tag
                it2.Selected = True
            End If
        End If
    Next i
    Call MyLockControls(False)
End Sub

Private Sub cmdExcute_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "リストが空です！", vbInformation Or vbOKOnly, "エラー"
        Exit Sub
    End If
    Call MyLockControls(True)
    If chkDoubleChange.Value = vbChecked Then
        Call DoubleChangeFilesName(False)
    Else
        Call ChangeFilesName(False)
    End If
    Call MyLockControls(False)

End Sub

Private Sub cmdRemove_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "リストが空です！", vbInformation Or vbOKOnly, "エラー"
        Exit Sub
    End If
    Call MyLockControls(True)
    Dim it As ListItem
    Dim i As Integer
    For i = ListView1.ListItems.Count To 1 Step -1
        Set it = ListView1.ListItems.Item(i)
        If it.Selected Then
            If i < ListView1.ListItems.Count Then
                ListView1.ListItems.Remove i
            End If
        End If
    Next i
    Call MyLockControls(False)
End Sub

Private Sub cmdRestore_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "リストが空です！", vbInformation Or vbOKOnly, "エラー"
        Exit Sub
    End If
    Call MyLockControls(True)
    Call RestoreChangeFilesName
    Call MyLockControls(False)
End Sub

Private Sub cmdTest_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "リストが空です！", vbInformation Or vbOKOnly, "エラー"
        Exit Sub
    End If
    Call MyLockControls(True)
    If chkDoubleChange.Value = vbChecked Then
        Call DoubleChangeFilesName(True)
    Else
        Call ChangeFilesName(True)
    End If
    Call MyLockControls(False)
End Sub

Private Sub cmdUp_Click()
    If ListView1.ListItems.Count = 0 Then
        MsgBox "リストが空です！", vbInformation Or vbOKOnly, "エラー"
        Exit Sub
    End If
    Call MyLockControls(True)
    Dim it As ListItem, it2 As ListItem, sit As ListSubItem
    Dim i As Integer
    For i = 1 To ListView1.ListItems.Count
        Set it = ListView1.ListItems.Item(i)
        If it.Selected Then
            If i > 1 Then
                ListView1.ListItems.Remove i
                Set it2 = ListView1.ListItems.Add(Index:=i - 1, Key:=it.Key, Text:=it.Text)
                Set sit = it.ListSubItems("AFTER")
                it2.ListSubItems.Add Index:=sit.Index, Key:=sit.Key, Text:=sit.Text
                Set sit = it.ListSubItems("STATUS")
                it2.ListSubItems.Add Index:=sit.Index, Key:=sit.Key, Text:=sit.Text
                it2.Tag = it.Tag
                it2.Selected = True
            End If
        End If
    Next i
    Call MyLockControls(False)
End Sub

Private Sub ListView1_OLEDragDrop(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
    If Data.GetFormat(MSComctlLib.ccCFFiles) = False Then
        Exit Sub
    End If
    
    Call MyLockControls(True)
    
    Dim v As Variant
    Dim it As ListItem
    Dim pt As String
    
    For Each v In Data.Files
        pt = CStr(v)
        If FSO.FileExists(pt) Then
            On Error Resume Next
            Err.Clear
            Set it = ListView1.ListItems.Add(Key:=pt, Text:=FSO.GetBaseName(pt))
            If Err.Number <> 0 Then
                If Err.Number <> ccNonUniqueKey Then
                    Err.Raise Err.Number
                Else
                    Err.Clear
                End If
            Else
                it.ListSubItems.Add Index:=ListView1.ColumnHeaders("AFTER").SubItemIndex, Key:="AFTER", Text:=""
                it.ListSubItems.Add Index:=ListView1.ColumnHeaders("STATUS").SubItemIndex, Key:="STATUS", Text:=""
                it.Tag = pt
            End If
            On Error GoTo 0
        End If
    Next v
    
    mblnExcuted = False
    
    Call MyLockControls(False)
    
End Sub

Private Sub txtBeforeNumberLength_LostFocus()
    Call FormatTextNumber(txtBeforeNumberLength, 10, 1)
End Sub

Private Sub txtNumberLength_LostFocus()
    Call FormatTextNumber(txtNumberLength, 5, 1)
End Sub

Private Sub txtStartNumber_LostFocus()
    Call FormatTextNumber(txtStartNumber, 10000, 1)
End Sub
